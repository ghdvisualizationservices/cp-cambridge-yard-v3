﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AirMonitoring : MonoBehaviour
{
    public float oxyValue;
    public float[] lelValues;
    public float h2sValue;
    public float coValue;
    public float[] vocValues;

    float updateTimer;

    [HideInInspector]
    public bool updating;

    GameObject airmonitor;

    Transform airMonitorValuesParent;

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag.Equals("Player"))
        {
            airmonitor = col.GetComponent<PlayerManager>().airmonitor;
            airMonitorValuesParent = airmonitor.transform.GetChild(0).GetChild(1).transform;
            airMonitorValuesParent.GetChild(0).GetComponent<Text>().text = oxyValue.ToString();
            airMonitorValuesParent.GetChild(1).GetComponent<Text>().text = Random.Range(lelValues[0], lelValues[1]).ToString("F1");
            airMonitorValuesParent.GetChild(2).GetComponent<Text>().text = h2sValue.ToString();
            airMonitorValuesParent.GetChild(3).GetComponent<Text>().text = coValue.ToString();
            airMonitorValuesParent.GetChild(4).GetComponent<Text>().text = Random.Range(vocValues[0], vocValues[1]).ToString("F1");

            FindObjectOfType<PlayerManager>().inZone = true;
        }
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.tag.Equals("Player") && !FindObjectOfType<PlayerManager>().CheckZonesUpdating())
        {
            if (!updating)
            {
                Timing.RunCoroutine(UpdateValues(col));
            }
        }
    }

    IEnumerator<float> UpdateValues(Collider col)
    {
        updating = true;
        yield return Timing.WaitForSeconds(2.0f);
        airmonitor = col.GetComponent<PlayerManager>().airmonitor;
        airMonitorValuesParent = airmonitor.transform.GetChild(0).GetChild(1).transform;
        airMonitorValuesParent.GetChild(0).GetComponent<Text>().text = oxyValue.ToString();
        airMonitorValuesParent.GetChild(1).GetComponent<Text>().text = Random.Range(lelValues[0], lelValues[1]).ToString("F1");
        airMonitorValuesParent.GetChild(2).GetComponent<Text>().text = h2sValue.ToString();
        airMonitorValuesParent.GetChild(3).GetComponent<Text>().text = coValue.ToString();
        airMonitorValuesParent.GetChild(4).GetComponent<Text>().text = Random.Range(vocValues[0], vocValues[1]).ToString("F1");
        updating = false;
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag.Equals("Player") && !FindObjectOfType<PlayerManager>().CheckIfInZone())
        {
            StopAllCoroutines();
            airmonitor = col.GetComponent<PlayerManager>().airmonitor;
            airMonitorValuesParent = airmonitor.transform.GetChild(0).GetChild(1).transform;
            airMonitorValuesParent.GetChild(0).GetComponent<Text>().text = "20.9";
            airMonitorValuesParent.GetChild(1).GetComponent<Text>().text = "0";
            airMonitorValuesParent.GetChild(2).GetComponent<Text>().text = "0";
            airMonitorValuesParent.GetChild(3).GetComponent<Text>().text = "0";
            airMonitorValuesParent.GetChild(4).GetComponent<Text>().text = "0";

            FindObjectOfType<PlayerManager>().inZone = false;
            updating = false;
        }
    }
}
