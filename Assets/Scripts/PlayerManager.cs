﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public bool inZone = true;

    public GameObject airmonitor;
    private GameObject[] airMonitoringZones;

    private Text[] airMonitorText;

    public AudioSource airMonitorSource;
    public AudioClip beepSound;

    public Material airMonitorMaterialOriginal;
    public Material airMonitorMaterialBeep1;
    public Material airMonitorMaterialBeep2;

    bool holdingAirMonitor;
    float beepTimer;

    // Start is called before the first frame update
    void Start()
    {
        airMonitorText = airmonitor.transform.GetChild(0).GetChild(1).GetComponentsInChildren<Text>();
        airMonitoringZones = GameObject.FindGameObjectsWithTag("Zone");
    }

    private void Update()
    {
        if (!CheckIfInZone())
        {
            airMonitorText[0].text = "20.9";
            airMonitorText[1].text = "0";
            airMonitorText[2].text = "0";
            airMonitorText[3].text = "0";
            airMonitorText[4].text = "0";
        }

        // Beeps airmonitor over 25 for VOCs
        if (float.Parse(airMonitorText[4].text) >= 20.0f) //&& holdingAirMonitor)
        {
            beepTimer += Time.deltaTime;
            if (beepTimer > 1.0f)
            {
                StartCoroutine(Play3Beeps());
                beepTimer = 0;
            }
        }
    }

    public bool CheckIfInZone()
    {
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            foreach (GameObject z in airMonitoringZones)
            {
                if (GameObject.FindGameObjectWithTag("Player").GetComponent<BoxCollider>().bounds.Intersects(z.GetComponent<BoxCollider>().bounds))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool CheckZonesUpdating()
    {
        foreach (GameObject z in airMonitoringZones)
        {
            if (z.GetComponent<AirMonitoring>().updating)
            {
                return true;
            }
        }
        return false;
    }

    public void BeepAirMonitor()
    {
        StartCoroutine(Play3Beeps());
    }

    IEnumerator Play3Beeps()
    {
        yield return new WaitForSeconds(0.125f);
        airMonitorSource.PlayOneShot(beepSound);
        airMonitorSource.GetComponent<MeshRenderer>().material = airMonitorMaterialBeep1;
        yield return new WaitForSeconds(0.125f);
        airMonitorSource.PlayOneShot(beepSound);
        airMonitorSource.GetComponent<MeshRenderer>().material = airMonitorMaterialBeep2;
        yield return new WaitForSeconds(0.125f);
        airMonitorSource.PlayOneShot(beepSound);
        airMonitorSource.GetComponent<MeshRenderer>().material = airMonitorMaterialBeep1;
        yield return new WaitForSeconds(0.125f);
        airMonitorSource.GetComponent<MeshRenderer>().material = airMonitorMaterialOriginal;
    }

    public void ToggleHoldingAirMonitor(bool hold)
    {
        holdingAirMonitor = hold;
    }
}
